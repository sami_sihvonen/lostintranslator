import React from 'react'
import { useState, useContext } from 'react'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import images from '../utils/signImages'
import { addTranslation } from '../api/translationsAPI'
import { useUser } from '../../context/UserContext'
import withAuth from '../../hoc/withAuth'
import { storageSave } from './../utils/storage'
import { STORAGE_KEY_USER } from '../../const/storageKeys'
import { Row } from 'react-bootstrap'
import Logo from '../../assets/Logo.png'

const TranslatorView = (props) => {
  const [newMessage, setNewMessage] = useState('')
  const [translatedSigns, setSigns] = useState([])

  const { user, setUser } = useUser()

  const translateMessage = (message) => {
    //reset last message from hook
    setSigns([])

    const messageArray = Array.from(message)
    let counter = 0

    //for every chra in message get corresponding sign image from images
    for (let char of messageArray) {
      const charAsNumber = char.charCodeAt(0) - 97

      const charObject = {
        image: images[charAsNumber],
        id: counter
      }
      counter++

      //add correct sign to state hook
      setSigns((translatedSigns) => translatedSigns.concat(charObject))
    }
  }

  const handleMessage = async (event) => {
    event.preventDefault()

    const [error, result] = await addTranslation(user, newMessage)
    console.log(result, 'addTranslation')
    setUser(result)
    storageSave(STORAGE_KEY_USER, result)
    //translate message to signs
    translateMessage(newMessage)
    setNewMessage('')
  }

  const handleMessageChange = (event) => {
    setNewMessage(event.target.value)
  }

  return (
    <Row className="translator__view">
      <Col>
        <h3>Translate a message</h3>
        <Form onSubmit={handleMessage} className="translate">
          <input value={newMessage} onChange={handleMessageChange} />
          <Button type="submit">Translate</Button>
        </Form>
        <Col className="imageList">
          {translatedSigns.map((sign) => (
            <div key={sign.id} className="sign">
              <img src={sign.image} alt={sign.id} />
            </div>
          ))}
        </Col>
        <img src={Logo} alt="logo" className="translate_logo" />
      </Col>
    </Row>
  )
}

export default withAuth(TranslatorView)
