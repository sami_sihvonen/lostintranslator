import React, { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { Button } from 'react-bootstrap'
import SendIcon from '@mui/icons-material/Send'
import { useNavigate } from 'react-router-dom'
import { loginUser } from '../api/userAPI'
import Logo from '../../assets/Logo.png'
import { storageSave } from './../utils/storage'
import { useUser } from '../../context/UserContext'
import { STORAGE_KEY_USER } from './../../const/storageKeys'


const API_URL = process.env.REACT_APP_API_URL

const usernameConfig = {
  requider: true,
  minLength: 2
}

const LoginView = () => {
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm()

  const { user, setUser } = useUser()
  const navigate = useNavigate()

  //Local State
  const [loading, setLoading] = useState(false)
  const [apiError, setApiError] = useState(null)

  // Side Effects
  useEffect(() => {
    if (user !== null) {
      navigate('profile')
    }
  }, [user, navigate])

  // useEffect(() => {
  //   console.log('Username', user)
  // })
  //Event handlers
  const onSubmit = async ({ username }) => {
    setLoading(true)
    const [error, userResponse] = await loginUser(username)
    if (error !== null) {
      setApiError(error)
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse)
      setUser(userResponse)
    }
    setLoading(false)
  }

  // Render functions
  const errorMessage = (() => {
    if (!errors.username) {
      return null
    }
    if (errors.username.type === 'required') {
      return <span>Username is required</span>
    }
    if (errors.username.type === 'minLength') {
      return <span>Username is too short (min.3)</span>
    }
  })()

  return (
    <form className="col-12 login" onSubmit={handleSubmit(onSubmit)}>
      <img src={Logo} alt="logo" />
      <h1 className="large text-light">What is you name?</h1>
      <fieldset>
        <label hidden htmlFor="username">
          Username:
        </label>
        <input
          type="text"
          {...register('username', usernameConfig)}
          className="form-control"
          placeholder="Name..."
        />
      </fieldset>
      <Button type="submit" className="btn btn-primary" disabled={loading}>
        LOGIN <SendIcon />
      </Button>
      {errorMessage}
      {loading && <p>Logging in...</p>}
      {apiError && <p>{apiError}</p>}
    </form>
  )
}

export default LoginView
