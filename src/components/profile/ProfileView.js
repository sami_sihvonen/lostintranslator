import React, { useEffect, useState } from 'react'
import { useUser } from '../../context/UserContext'
import withAuth from '../../hoc/withAuth'
import { userById } from '../api/userAPI'
import ProfileActions from './ProfileActions'
import ProfileHeader from './ProfileHeader'
import ProfileMessageHistory from './ProfileMessageHistory'
import { storageSave } from './../utils/storage'
import { STORAGE_KEY_USER } from '../../const/storageKeys'

const ProfileView = () => {
  const { user, setUser } = useUser()

  

  useEffect(() => {
    ;(async function () {
      const [error, latestUser] = await userById(user.id)

      if (error == null) {
        storageSave(STORAGE_KEY_USER, latestUser)
        setUser(latestUser)
      }
    })()
  }, [setUser, user.id])

  const messagesLast10 = user.translations.slice(-10).reverse()

  console.log(user.translations, 'user translations')
  return (
    <div>
      <ProfileHeader username={user.username} />
      <ProfileActions />
      <ProfileMessageHistory translations={messagesLast10} />
    </div>
  )
}

export default withAuth(ProfileView)
