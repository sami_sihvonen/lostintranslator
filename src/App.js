import React, { useState, useContext, useEffect } from 'react'
import LoginView from './components/login/LoginView'
import ProfileView from './components/profile/ProfileView'
import TranslatorView from './components/translator/TranslatorView'
import NotFound from './components/notfound/NotFound'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Header from './components/header/Header'
import { Container } from 'react-bootstrap'
import { useUser } from './context/UserContext'

const App = () => {
  return (
    <Router>
      <Header />
      <Container>
        <div className="main">
          <Routes>
            <Route path="/" element={<LoginView />} />
            <Route path={`/profile`} element={<ProfileView />} />
            <Route path="/translator" element={<TranslatorView />} />
          </Routes>
        </div>
      </Container>
    </Router>
  )
}

export default App
