## Assignment 2:  Translator App with React


## Table of Contents

- [Background](#background)
- [Install](#install)
- [Maintainers](#maintainers)

## Background

This is a responsive web application made with Javascript and React. In the app user can sign in using their username.
They can translate words and messages to sign language. This app uses a restApi to store and get user and translation information.

This project is hosted on heroku: https://lostintranslator.herokuapp.com/

This exercise was part of Noroff School of Technology and Experis Academy Fullstack Developer-course

### Usage

- Log in: User can sign in usign their username 
- Translate: User can input words and messages (without spaces and special characters. These are translated to sign language.
- Profile: Users can view their last ten translations (as text). Users can also delete their message history.
- Logout: User can logout from the application.


## Install

Clone repository `git clone`

- npm install

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.


## Maintainers

[@Laura Byman](https://gitlab.com/Laura_Byman)
[@Sami Sihvonen](https://gitlab.com/sami_sihvonen)
